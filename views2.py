from django.shortcuts import render
from .forms import RegForm
from .models import Adquisiciones
# Create your views here.

def inicio(request):
    form = RegForm(request.POST or None)
    if form.is_valid():
        form_data = form.cleaned_data
        nombre_producto2 = form_data.get("nombre_producto")
        nombre_proveedor2 = form_data.get("nombre_proveedor")
        cantidad2 = form_data.get("cantidad")
        numero_factura2 = form_data.get("numero_factura")

        objeto = Adquisiciones.objects.create(nombre_producto=nombre_producto2, nombre_proveedor=nombre_proveedor2, cantidad=cantidad2, numero_factura=numero_factura2)

    contexto = {
        "el_formulario": form,
    }

    return render(request, "inicio2.html", contexto)
