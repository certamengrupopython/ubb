from django import forms
from .models import Producto

class ProModelForm(forms.ModelForm):
    class Meta:
        modelo = Producto
        campos = ["nombre","descripcion","categoria","sub_categoria"]


class ProForm(forms.Form):
    nombre = forms.CharField(max_length=100)
    descripcion = forms.CharField()
    categoria = forms.CharField()
    sub_categoria = forms.CharField()

    class Meta:
        model = Producto
        fields = ["nombre","descripcion","categoria","sub_categoria"]

    def clean_nombre(self):
        nombre = self.cleaned_data.get("nombre")
        if nombre:
            if len(nombre) <= 1:
                raise forms.ValidationError("El nombre del producto  no puede ser de un caracter")
            return nombre
        else:
            raise forms.ValidationError("Este campo es obligatorio")

    def clean_descripcion(self):
        descripcion = self.cleaned_data.get("descripcion")
        if descripcion:
            if len(descripcion) <= 1:
                raise forms.ValidationError("La descripcion del producto  no puede ser de un caracter")
            return descripcion
        else:
            raise forms.ValidationError("Este campo es obligatorio")

    def clean_categoria(self):
        categ = self.cleaned_data.get("categoria")
        if categ:
            if len(categ) <= 1:
                raise forms.ValidationError("La categoria del producto  no puede ser de un caracter")
            return categ
        else:
            raise forms.ValidationError("Este campo es obligatorio")

    def clean_sub_categoria(self):
        sub_categ = self.cleaned_data.get("sub_categoria")
        if sub_categ:
            if len(sub_categ) <= 1:
                raise forms.ValidationError("La sub_categoria del producto  no puede ser de un caracter")
            return sub_categ
        else:
            raise forms.ValidationError("Este campo es obligatorio")

class ProModelForm(forms.ModelForm):
    class Meta:
        modelo = Producto
        fields = ["nombre","descripcion","categoria","sub_categoria"]

    def clean_nombre(self):
        nombre = self.cleaned_data.get("nombre")
        if nombre:
            if len(nombre) <= 1:
                raise forms.ValidationError("El nombre del producto  no puede ser de un caracter")
            return nombre
        else:
            raise forms.ValidationError("Este campo es obligatorio")

    def clean_descripcion(self):
        descripcion = self.cleaned_data.get("descripcion")
        if descripcion:
            if len(descripcion) <= 1:
                raise forms.ValidationError("La descripcion del producto  no puede ser de un caracter")
            return descripcion
        else:
            raise forms.ValidationError("Este campo es obligatorio")

    def clean_categoria(self):
        categ = self.cleaned_data.get("categoria")
        if categ:
            if len(categ) <= 1:
                raise forms.ValidationError("La categoria del producto  no puede ser de un caracter")
            return categ
        else:
            raise forms.ValidationError("Este campo es obligatorio")

    def clean_sub_categoria(self):
        sub_categ = self.cleaned_data.get("sub_categoria")
        if sub_categ:
            if len(sub_categ) <= 1:
                raise forms.ValidationError("La sub_categoria del producto  no puede ser de un caracter")
            return sub_categ
        else:
            raise forms.ValidationError("Este campo es obligatorio")

