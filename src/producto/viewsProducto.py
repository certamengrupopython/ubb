from django.shortcuts import render
from .forms import ProForm
from .models import Producto


def inicioProducto(request):
    form = ProForm(request.POST or None)

    if form.is_valid():
        form_data = form.cleaned_data
        nombre2 = form_data.get("nombre")
        descripcion2 = form_data.get("descripcion")
        categoria2 = form_data.get("categoria")
        sub_categoria2 = form_data.get("sub_categoria")
        objeto = Producto.objects.create(nombre=nombre2, descripcion=descripcion2, categoria=categoria2, sub_categoria=sub_categoria2)

    contexto = {
        "el_formulario": form,
    }

    return render(request, "producto.html", contexto)







# Create your views here.
