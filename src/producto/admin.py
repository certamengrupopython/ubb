from django.contrib import admin
from producto.models import Producto
from .forms import ProForm, ProModelForm


class AdminProducto(admin.ModelAdmin):
    list_display = ["nombre","descripcion","categoria","sub_categoria"]
    form = ProModelForm
    list_filter = ["nombre"]
    search_fields = ["nombre"]
    class Meta:
        model = Producto



admin.site.register(Producto, AdminProducto)
