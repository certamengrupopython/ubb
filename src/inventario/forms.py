from django import forms
from .models import Inventario


class InvenForm(forms.Form):
    nombre_producto = forms.CharField(max_length=100)
    cantidad_producto = forms.IntegerField()
    pertenencia_primaria = forms.CharField()
    pertenencia_secundaria = forms.CharField()

    class Meta:
        model = Inventario
        fields = ["nombre_producto", "cantidad_producto", "pertenencia_primaria", "pertenencia_secundaria"]

    def clean_nombre_producto(self):
        nombre = self.cleaned_data.get("nombre_producto")
        if nombre:
            if len(nombre) <= 1:
                raise forms.ValidationError("El nombre no puede ser de un caracter")
            return nombre
        else:
            raise forms.ValidationError("Este campo es obligatorio")

    def clean_cantidad_producto(self):
        cantidad = self.cleaned_data.get("cantidad_producto")

        if cantidad < 0:
            raise forms.ValidationError("La cantidad de producto no puede ser negativo")
        elif cantidad == 0:
            raise forms.ValidationError("La cantidad de producto no puede ser cero")
        return cantidad

    def clean_pertenencia_primaria(self):
        pertenencia1 = self.cleaned_data.get("pertenencia_primaria")
        if pertenencia1:
            if len(pertenencia1) <= 1:
                raise forms.ValidationError("La pertenencia primaria no puede ser de un caracter")
            return pertenencia1
        else:
            raise forms.ValidationError("Este campo es obligatorio")

    def clean_pertenencia_secundaria(self):
        pertenencia2 = self.cleaned_data.get("pertenencia_secundaria")
        if pertenencia2:
            if len(pertenencia2) <= 1:
                raise forms.ValidationError("La pertenencia secundaria no puede ser de un caracter")
            return pertenencia2
        else:
            raise forms.ValidationError("Este campo es obligatorio")


class InveModelForm(forms.ModelForm):
    class Meta:
        modelo = Inventario
        fields = ["nombre_producto","cantidad_producto","pertenencia_primaria","pertenencia_secundaria"]

    def clean_nombre_producto(self):
        nombre = self.cleaned_data.get("nombre_producto")
        if nombre:
            if len(nombre) <= 1:
                raise forms.ValidationError("El nombre no puede ser de un caracter")
            return nombre
        else:
            raise forms.ValidationError("Este campo es obligatorio")

    def clean_cantidad_producto(self):
        cantidad = self.cleaned_data.get("cantidad_producto")

        if cantidad < 0:
            raise forms.ValidationError("La cantidad de producto no puede ser negativo")
        elif cantidad == 0:
            raise forms.ValidationError("La cantidad de producto no puede ser cero")
        return cantidad

    def clean_pertenencia_primaria(self):
        pertenencia1 = self.cleaned_data.get("pertenencia_primaria")
        if pertenencia1:
            if len(pertenencia1) <= 1:
                raise forms.ValidationError("La pertenencia primaria no puede ser de un caracter")
            return pertenencia1
        else:
            raise forms.ValidationError("Este campo es obligatorio")

    def clean_pertenencia_secundaria(self):
        pertenencia2 = self.cleaned_data.get("pertenencia_secundaria")
        if pertenencia2:
            if len(pertenencia2) <= 1:
                raise forms.ValidationError("La pertenencia secundaria no puede ser de un caracter")
            return pertenencia2
        else:
            raise forms.ValidationError("Este campo es obligatorio")
