from django.shortcuts import render
from .forms import InvenForm
from .models import Inventario




def inicioInventario(request):
    form = InvenForm(request.POST or None)

    if form.is_valid():
        form_data = form.cleaned_data
        nombre_producto2 = form_data.get("nombre_producto")
        cantidad_producto2 = form_data.get("cantidad_producto")
        pertenencia_primaria2 = form_data.get("pertenencia_primaria")
        pertenencia_secundaria2 = form_data.get("pertenencia_secundaria")
        objeto = Inventario.objects.create(nombre_producto=nombre_producto2, cantidad_producto = cantidad_producto2, pertenencia_primaria = pertenencia_primaria2 , pertenencia_secundaria = pertenencia_secundaria2)

    contexto = {
        "el_formulario" : form,
    }

    return render(request, "inventario.html", contexto)



# Create your views here.
