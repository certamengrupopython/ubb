from django.db import models
class Inventario( models.Model):
    id_inventario = models.AutoField(primary_key=True,unique=True)
    nombre_producto = models.CharField(max_length=100)
    cantidad_producto = models.PositiveIntegerField()
    pertenencia_primaria = models.TextField()
    pertenencia_secundaria = models.TextField()

    def __str__(self):
        return self.nombre_producto



# Create your models here.
