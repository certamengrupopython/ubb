from django.contrib import admin
from inventario.models import Inventario
from .forms import InvenForm,InveModelForm

class AdminInventario(admin.ModelAdmin):
    list_display = ["nombre_producto","cantidad_producto","pertenencia_primaria","pertenencia_secundaria"]
    form = InveModelForm
    list_filter = ["cantidad_producto"]
    list_editable = ["cantidad_producto"]
    search_fields = ["nombre_producto"]
    class Meta:
        model = Inventario




admin.site.register(Inventario,AdminInventario)

# Register your models here.
