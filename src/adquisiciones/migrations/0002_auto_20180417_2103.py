# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-04-18 00:03
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('adquisiciones', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='adquisiciones',
            name='cantidad',
            field=models.PositiveIntegerField(),
        ),
        migrations.AlterField(
            model_name='adquisiciones',
            name='numero_factura',
            field=models.PositiveIntegerField(),
        ),
    ]
