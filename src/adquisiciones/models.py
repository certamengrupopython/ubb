from django.db import models


# Create your models here.
class Adquisiciones(models.Model):
    id_adquisiciones = models.AutoField(primary_key=True,unique=True)
    nombre_producto = models.CharField(max_length=100, blank=True, null=True)
    nombre_proveedor = models.CharField(max_length=100, blank=True, null=True)
    cantidad = models.PositiveIntegerField()
    fecha = models.DateTimeField(auto_now_add=True, auto_now=False)
    numero_factura = models.PositiveIntegerField()


    def __str__(self):
        return self.nombre_producto

