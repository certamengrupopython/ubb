from django import forms
from .models import Adquisiciones

class RegForm(forms.Form):

    nombre_producto = forms.CharField(max_length=100)
    nombre_proveedor = forms.CharField(max_length=100)
    cantidad = forms.IntegerField()
    numero_factura = forms.IntegerField()

    class Meta:
        model = Adquisiciones
        fields=["nombre_producto","nombre_proveedor","cantidad","numero_factura"]


    def clean_nombre_producto(self):
        nombre = self.cleaned_data.get("nombre_producto")
        if nombre:
            if len(nombre)<=1:
                raise forms.ValidationError("El nombre no puede ser de un caracter")
            return nombre
        else:
            raise forms.ValidationError("Este campo es requerido")

    def clean_nombre_proveedor(self):
        proveedor = self.cleaned_data.get("nombre_proveedor")
        if proveedor:
            if len(proveedor)<2:
                raise forms.ValidationError("EL nombre del proveedor no puede ser de un caracter")
            return proveedor
        else:
            raise forms.ValidationError("Este campo es requerido")

    def clean_cantidad(self):
        stock = self.cleaned_data.get("cantidad")

        if stock < 0:
            raise forms.ValidationError("El stock minimo no puede ser negativo")
        elif stock == 0:
            raise forms.ValidationError("el stock minimo no puede ser cero")
        return stock

    def clean_numero_factura(self):
        factura = self.cleaned_data.get("numero_factura")

        if factura < 0:
            raise forms.ValidationError("La factura no puede ser negativ")
        elif factura == 0:
            raise forms.ValidationError("La factura no puede ser igual a cero")
        return factura

class AdquiModelForm(forms.ModelForm):
    class Meta:
        model = Adquisiciones
        fields=["nombre_producto","nombre_proveedor","cantidad","numero_factura"]


    def clean_nombre_producto(self):
        nombre = self.cleaned_data.get("nombre_producto")
        if nombre:
            if len(nombre)<=1:
                raise forms.ValidationError("El nombre no puede ser de un caracter")
            return nombre
        else:
            raise forms.ValidationError("Este campo es requerido")

    def clean_nombre_proveedor(self):
        proveedor = self.cleaned_data.get("nombre_proveedor")
        if proveedor:
            if len(proveedor)<2:
                raise forms.ValidationError("EL nombre del proveedor no puede ser de un caracter")
            return proveedor
        else:
            raise forms.ValidationError("Este campo es requerido")

    def clean_cantidad(self):
        stock = self.cleaned_data.get("cantidad")

        if stock < 0:
            raise forms.ValidationError("El stock minimo no puede ser negativo")
        elif stock == 0:
            raise forms.ValidationError("el stock minimo no puede ser cero")
        return stock

    def clean_numero_factura(self):
        factura = self.cleaned_data.get("numero_factura")

        if factura < 0:
            raise forms.ValidationError("La factura no puede ser negativ")
        elif factura == 0:
            raise forms.ValidationError("La factura no puede ser igual a cero")
        return factura