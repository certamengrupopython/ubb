from django.contrib import admin
from bodega.models import BodegaModel
from bodega.forms import BodegaModelForm

class AdminBodega(admin.ModelAdmin):
    list_display = ["id_bodega","nombre_bodega","stock_producto","stock_minimo","ubicacion","fecha_ingreso_bodega"]
    form = BodegaModelForm


admin.site.register(BodegaModel, AdminBodega)