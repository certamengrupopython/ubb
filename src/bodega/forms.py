from django import forms
from bodega.models import BodegaModel


class BodegaModelForm(forms.ModelForm):
    class Meta:
        model = BodegaModel
        fields=["nombre_bodega","stock_producto","stock_minimo","ubicacion","fecha_ingreso_bodega"]

    def clean_nombre_bodega(self):
        nombre = self.cleaned_data.get("nombre_bodega")
        if nombre:
            if len(nombre)<=1:
                raise forms.ValidationError("El nombre no puede ser de un caracter")
            return nombre
        else:
            raise forms.ValidationError("Este campo es requerido")

    def clean_ubicacion(self):
        ubicacion = self.cleaned_data.get("ubicacion")
        if ubicacion:
            if len(ubicacion)<2:
                raise forms.ValidationError("La ubicación no puede ser de un caracter")
            return ubicacion
        else:
            raise forms.ValidationError("Este campo es requerido")

    def clean_stock_minimo(self):
        stock = self.cleaned_data.get("stock_minimo")

        if stock < 0:
            raise forms.ValidationError("El stock minimo no puede ser negativo")
        elif stock == 0:
            raise forms.ValidationError("el stock minimo no puede ser cero")
        return stock

    def clean_stock_producto(self):

        stock1 = self.cleaned_data.get("stock_producto")

        if stock1 < 0:
            raise forms.ValidationError("El stock no puede ser negativo")
        elif stock1 == 0:
            raise forms.ValidationError("el stock no puede ser cero")
        return stock1